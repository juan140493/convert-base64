module.exports = {
  baseUrl: './',
  productionSourceMap: false,
  devServer: {
    port: 1234,
  },
};
